# -*- encoding: utf-8 -*-
# stub: grpc-tools 1.41.1 ruby lib

Gem::Specification.new do |s|
  s.name = "grpc-tools".freeze
  s.version = "1.41.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["grpc Authors".freeze]
  s.date = "2021-10-20"
  s.description = "protoc and the Ruby gRPC protoc plugin".freeze
  s.email = "grpc-io@googlegroups.com".freeze
  s.executables = ["grpc_tools_ruby_protoc".freeze, "grpc_tools_ruby_protoc_plugin".freeze]
  s.files = ["bin/grpc_tools_ruby_protoc".freeze, "bin/grpc_tools_ruby_protoc_plugin".freeze]
  s.homepage = "https://github.com/google/grpc/tree/master/src/ruby/tools".freeze
  s.licenses = ["Apache-2.0".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "Development tools for Ruby gRPC".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version
end
