FROM ruby:2.7

WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle install 

COPY . .

# Build the task/project/user service protos for Ruby
RUN grpc_tools_ruby_protoc \
    --proto_path=./proto \
    --ruby_out=./proto \
    --grpc_out=./proto \
    ./proto/task/*.proto ./proto/user/*.proto ./proto/project/*.proto


EXPOSE 8080

CMD ./init
